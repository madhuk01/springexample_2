<%-- 
    Document   : confirm
    Created on : Aug 18, 2016, 12:09:57 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}/SpringExample" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>Confirm Page</title>
    </head>
    <body>
        <p>Data inserted !!</p>
        <p><a href="${contextPath}/SpringExample/">Back to homepage</a></p>
    </body>
</html>
