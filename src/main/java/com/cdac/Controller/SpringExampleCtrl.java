/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.Controller;

import com.cdac.Model.Person;
import com.cdac.Service.SpringExService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author user
 */
@Controller
public class SpringExampleCtrl {

    private SpringExService springexService;

    @Autowired(required = true)
    @Qualifier(value = "springexService")
    public void setSpringexService(SpringExService springexService) {
        this.springexService = springexService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homePage(Model model) {

        model.addAttribute("person", new Person());

        return "index";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addData(@ModelAttribute("person") Person person) {

        ModelAndView modelandview = new ModelAndView();

        System.out.println("ID: " + person.getId() + " ::: " + "Name: " + person.getName());

        // this.springexService.addPerson(person);
        if (person.getId() == 0) {
            this.springexService.addPerson(person);
        } else {
            this.springexService.updatePerson(person);
        }

        modelandview.setViewName("confirm");

        return modelandview;
    }

    @RequestMapping(value = "/getlist", method = RequestMethod.GET)
    public String listPersons(Model model) {
        // model.addAttribute("person", new Person());
        model.addAttribute("listP", this.springexService.listPersons());
        List<Person> plist = this.springexService.listPersons();
        for (Person p : plist) {
            System.out.println("person names:::" + p.getName());
        }
        return "getlist";
    }

    @RequestMapping(value = "/del/{ids}", method = RequestMethod.GET)
    public String deletePersons(@PathVariable String ids, Model model) {

        String[] ids1 = ids.split(",");

        this.springexService.removePerson(ids1);

        model.addAttribute("message", "Deleted successfully");

        return "getlist";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editPersons(@PathVariable("id") int id, Model model) {

        // Person p = this.springexService.getPersonById(id);
       // this.springexService.updatePerson(p);
        
        
        model.addAttribute("person", this.springexService.getPersonById(id));

        model.addAttribute("listpersons", this.springexService.listPersons());

        // model.addAttribute("message", "Deleted successfully");
        return "index";
    }

}
    