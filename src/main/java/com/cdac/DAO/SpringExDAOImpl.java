/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.DAO;

import com.cdac.Model.Person;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author user
 */
@Repository
public class SpringExDAOImpl implements SpringExDAO {
    
    private static final Logger logger = LoggerFactory.getLogger(SpringExDAOImpl.class);
  

    private  SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addPerson(Person p) {
         //To change body of generated methods, choose Tools | Templates.
         
        
         System.out.println("Inside DAO method");
         
         System.out.print("Data::");
         
         System.out.println(p.getId()+" "+p.getEmail()+" "+p.getName()+" "+p.getPhoneno());
         
         Session s1 = this.sessionFactory.getCurrentSession();
         
         s1.persist(p);

    }

    @Override
    public void updatePerson(Person p) {
        
        Session session = this.sessionFactory.getCurrentSession();
	session.update(p);
	logger.info("Person updated successfully, Person Details="+p);
  
    }

    @Override
    public List<Person> listPersons() {
         //To change body of generated methods, choose Tools | Templates.
         Session s2 = this.sessionFactory.getCurrentSession();
         
         List<Person> personsList = s2.createQuery("from Person").list();
         
        /* for(Person p : personsList){
                    System.out.println("Name: "+p.getName()+"Email: "+p.getEmail()+"Phone: "+p.getPhoneno());
		} */
         
         return personsList;
    }

    @Override
    public Person getPersonById(int id) {
        
        Session s2 = this.sessionFactory.openSession();
        
        Person p = (Person) s2.load(Person.class, id);
        
        return p;
        
    }

    @Override
    public void removePerson(String[] ids) {
         //To change body of generated methods, choose Tools | Templates.
         
              Session session = this.sessionFactory.getCurrentSession();
                
        for (String id1 : ids) {
            int id = Integer.parseInt(id1);
            Person p = (Person) session.load(Person.class, id);
            if(null != p){
              
                session.delete(p);
               // session.createQuery("alter table sampletable auto_increment = 0");
            }
            logger.info("Person deleted successfully, person details="+p);
        }  
    }
    
}
