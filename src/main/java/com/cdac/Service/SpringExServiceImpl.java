/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.Service;

import com.cdac.DAO.SpringExDAO;
import com.cdac.Model.Person;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author user
 */
@Service
public class SpringExServiceImpl implements SpringExService {
    
   
    private SpringExDAO springexDAO;

    public void setSpringexDAO(SpringExDAO springexDAO) {
        this.springexDAO = springexDAO;
    }
    
    
    

    @Override
    @Transactional
    public void addPerson(Person p) {
         //To change body of generated methods, choose Tools | Templates.
         
         System.out.println("Inside service method");
         
         this.springexDAO.addPerson(p);
    }

    @Override
    @Transactional
    public void updatePerson(Person p) {
        
        this.springexDAO.updatePerson(p);
    }

    @Override
    @Transactional
    public List<Person> listPersons() {
         //To change body of generated methods, choose Tools | Templates.
         List<Person> personsList = this.springexDAO.listPersons();
         
         System.out.println("Length:: "+personsList.size());
         
         return personsList;   
    }

    @Override
    @Transactional
    public Person getPersonById(int id) {
            Person p = (Person) this.springexDAO.getPersonById(id);
            return p;
    }

    @Override
    @Transactional
    public void removePerson(String[]  ids) {
         //To change body of generated methods, choose Tools | Templates.
         
         this.springexDAO.removePerson(ids);
         
    }
    
}
