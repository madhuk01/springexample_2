/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.Service;

import com.cdac.Model.Person;
import java.util.List;

/**
 *
 * @author user
 */
public interface SpringExService {
    
        public void addPerson(Person p);
	public void updatePerson(Person p);
	public List<Person> listPersons();
	public Person getPersonById(int id);
	public void removePerson(String[] ids);
    
}
