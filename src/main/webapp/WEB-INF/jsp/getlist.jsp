<%-- 
    Document   : getlist
    Created on : Aug 19, 2016, 10:02:24 AM
    Author     : user
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>Members List</title>
        <script type="text/javascript">
            
            function deleteSelected() {
                    var length = ${listP.size()};
                    var selectedPersons = [];
                    var k = 0;
                   for(var i=0;i< length;i++) {
                       
                    //  var id =  "del"+(i+1);
                    
                    var id = document.getElementsByClassName("checkbox")[i].id;
                    
                    console.log("Id: "+id);
                      
                      var checkboxid = document.getElementById(id).checked;
                      
                      console.log("checkbox id: "+checkboxid);

                       if(checkboxid === true)
                       {
                           id = id.replace(/[^0-9]+/ig,"");
            
                           selectedPersons[k] = parseInt(id);
                           
                          console.log("234: "+selectedPersons[k]);
                           
                           k++;
                       }
                        
                    }
                  // alert("No of selected items: "+selectedPersons.length);
                  
                  
                  var url = "${contextPath}/del/"+selectedPersons;
                  
                  var request;
                  if(window.XMLHttpRequest){
                      
                     request  = new XMLHttpRequest();
                    
                  }
                  else {
                      request = new ActiveXObject("Microsoft.XMLHTTP");
                      
                  }
                  
                  request.open('GET',url,true);
                  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                   request.onreadystatechange = function() {
                    if (request.readyState === 4 && request.status === 200) {
                      // callback(request);
                      
                      document.getElementById("msg").innerHTML = "Deleted successfully";
                      
                      location.reload();
                      
                    }
                  };
                  request.send(selectedPersons);                  
            }        
        </script>
    </head>
    <body>
     <c:if test="${not empty listP}">
            <table border="1">
            <tr>
                    <th width="80">Person ID</th>
                    <th width="120">Person Name</th>
                    <th width="120">Person Email</th>
                    <th width="180">Person Phone Number</th>
                    <th width="80">Delete</th>
                    <th width="80">Edit Person</th>
            </tr>
            
            <c:forEach var="k" items="${listP}" varStatus="status">
                    <tr>
                            <td>${k.id}</td>
                            <td>${k.name}</td>
                            <td>${k.email}</td>
                            <td>${k.phoneno}</td>
                            <td align='center'><input type="checkbox" class="checkbox" id="del${k.id}"></input></td>
                            <td align='center'><a href="<c:url value="/edit/${k.id}"/>">Edit</a></td>
                            
                    </tr>
            </c:forEach>
            </table>
            
            <br><br>
             <button type="button" onclick="javascript:deleteSelected();">Delete selected</button>
             <br><br>
                <p><a href="${contextPath}/">Back to homepage</a></p>
             
             
             <p id="msg"></p>
            
    </c:if>
        
    </body>
</html>
