<%-- 
    Document   : index
    Created on : Aug 18, 2016, 11:33:57 AM
    Author     : user
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="windows-1252"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>Home Page</title>
    </head>
    <body>
        <h1>Sample App</h1>
        <form:form name="sampleform" method="POST" action="${contextPath}/add" commandName="person">
            <table>
                
                <tr>
                    <form:hidden path="id" />
                </tr>
                <tr>
                    <td>Name :</td>
                    <td>
                        <form:label path="name">
                            <form:input type="text" id="name" path="name"/>
                        </form:label> 
                        
                    
                    </td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td>
                        <form:label path="email">
                            <form:input type="text" id="email" path="email"/></td>                            
                        </form:label>
                        
                </tr>
                <tr>
                    <td>Phone Number: </td>
                    <td>
                        <form:label path="phoneno">
                            <form:input type="text" id="phoneno" path="phoneno" />
                        </form:label>
                       
                    </td>
                </tr> 
                <tr>
                    <td></td>
                    <td><input type="submit" value="Submit"/></td>
                </tr>
            </table>
        </form:form>
        
        <br><br>
        
        <a href="<c:url value="/getlist"/>">Get all Members List</a>
        
    </body>
</html>
